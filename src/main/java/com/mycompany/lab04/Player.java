/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

/**
 *
 * @author Leywin
 */
public class Player {

    private char symbol;
    private int winCount, drawCount, loseCount;
    public Player(char symbol) {
        this.symbol = symbol;
        this.winCount = 0;
        this.drawCount = 0;
        this.loseCount = 0;
    }

    public int getWinCount() {
        return winCount;
    }
    public void win(){
        winCount++;
    }
    public void lose(){
        loseCount++;
    }
    public void draw(){
        drawCount++;
    }
  
    public int getDrawCount() {
        return drawCount;
    }

    public int getLoseCount() {
        return loseCount;
    }

    public char getSymbol() {
        return symbol;
    }
    @Override
    public String toString() {
        return "Player{" + "symbol=" + symbol + ", winCount=" + winCount + ", drawCount=" + drawCount + ", loseCount=" + loseCount + '}';
    }
}
