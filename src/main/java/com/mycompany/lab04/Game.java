/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab04;

import java.util.Scanner;

/**
 *
 * @author Leywin
 */
public class Game {

    private Player player1;
    private Player player2;
    private Table table;

    public Game() {
        this.player1 = new Player('O');
        this.player2 = new Player('X');
    }
    
    public void newGame(){
        this.table = new Table(player1,player2);
    }
    public void play() {
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                showInfo();
                printWin();
                newGame();
            }
            if(table.checkDraw()){
                showTable();
                showInfo();
                newGame();
            }
            table.switchPlayer();
        }
    }   

    private void showWelcome() {
        System.out.println("Welcome to OX Game!");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println("");
        }
    }
    

    private void showTurn() {
        System.out.println("Turn " + table.getCurrentPlayer().getSymbol());
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        while (true) {
            System.out.print("Please enter row col: ");
            int row = kb.nextInt();
            int col = kb.nextInt();
            if (table.setRowCol(row, col)){
                break;
            }
            System.out.println("");
        }
    }

    private void switchPlayer() {

    }

    private void printWin() {
        System.out.println("win!!");
    }

    private void showInfo() {
        System.out.println(player1);
        System.out.println(player2);
    }

}
